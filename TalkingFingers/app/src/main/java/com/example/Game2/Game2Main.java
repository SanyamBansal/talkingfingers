package com.example.Game2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.talkingfingers.R;


public class Game2Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_game2);
    }

    // when start button is pressed //
    public void start_game(View view)
    {
        Intent i = new Intent(Game2Main.this, Game2Activity.class);
        startActivity(i);
    }

}
